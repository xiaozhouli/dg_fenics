"""This code solves 2D linear convection equation
    u_t + dot(a, grad(u)) = f(x, y), 
on the unit square with source f given by
    f(x, y) = 0
and Dirichlet boundary conditions given by
    u(x, 0, t) = sin(2*pi*(x-(ax + ay)*t)), 
    u(0, y, t) = sin(2*pi*(y-(ax + ay)*t))
initial condition
    u(x, y, 0) = sin(2*pi*(x+y))
using a discontinuous Galerkin formulation. 
For a = (ax, ay), the exact solution
    u(x, y, t) = sin(2*pi*(x + y - (ax + ay)*t))
Note:
    works well for the degree from 1 to 4
    time integrator: theta method (explicit, implicit, improved Euler methods)
        1) solve
        2) matrix 
        it is one order integrator, one have to keep cfl number very small to
        show the spatial accuracy order 
    numerical flux: 
        1) upwind
        2) average wind
        3) downwind (unstable)
        4) biupwind
FIXME:
    add the source term into the varation form
    proper set the quad_degree (minimum)
"""
"""
    by Xiaozhou Li, June 10, 2016 
"""
from dolfin import *
import numpy as np
import sys

set_log_level(30)
#set_log_active(False)
# Define parameter
tol = 1.e-14
ax = 1.0; ay = 1.0; a = Constant((ax,ay))
theta0 = 0.65 # used in biupwind flux, theta >= 0.5 (stable)
T  = 0.1
cfl = 0.1
solve_method = 'Matrix'
# Define the quadrature degree
quad_degree = 8
dx = dx(metadata={'quadrature_degree': quad_degree})

# Define the problem
def linear_convection(nx, ny, degree, flux='upwind', theta=1., norm='L2'):
    # Create mesh and define function space
    mesh = UnitSquareMesh(nx, ny)
    dt = cfl/nx 
    print (flux, degree, theta, dt)
    V_dg = FunctionSpace(mesh, 'DG', degree)
    
    # Define test and trial functions
    u = TrialFunction(V_dg)
    v = TestFunction(V_dg)

    # Define normal vector and mesh size
    n = FacetNormal(mesh)
    h = CellSize(mesh)
    h_avg = (h('+') + h('-')) / 2.


    # Define initial condition
    g0 = Expression('sin(2*pi*(x[0]+x[1]))', degree=2*degree)
    u0 = project(g0, V_dg)

    # Define boundary conditions
    gy = Expression('sin(2*pi*(x[0]-(ax+ay)*t))', ax=ax, ay=ay, t=0, degree=2*degree)
    gx = Expression('sin(2*pi*(x[1]-(ax+ay)*t))', ax=ax, ay=ay, t=0, degree=2*degree)
    
    #bcmethod = "geometric"
    def y_boundary(x, on_boundary):
        return on_boundary and x[0] < tol
    def x_boundary(x, on_boundary):
        return on_boundary and x[1] < tol
    inflow_y = DirichletBC(V_dg, gy, x_boundary, "geometric")
    inflow_x = DirichletBC(V_dg, gx, y_boundary, "geometric")
    bcs = [inflow_x, inflow_y]

    # Define the source function 
    f = Constant(0.0)

    # Define flux
    def Flux(u):
        if (flux == 'upwind'):
            f_hat = a * u('+')
            f_hat0 = a * u
        elif (flux == 'average'):
            f_hat = a * avg(u)
            f_hat0 = a * u
        elif (flux == 'downwind'):
            f_hat = a * u('-')
            f_hat0 = a * u
        elif (flux == 'biupwind'):
            f_hat = a * (theta0*u('+') + (1 - theta0)*u('-'))
            f_hat0 = a * u

        return f_hat, f_hat0

    def Mass(u):
        return u*v*dx

    def RHS(u):
        f_hat, f_hat0 = Flux(u)
        return inner(a*u, grad(v))*dx \
            - inner(a*u('+'), jump(v,n))*dS - inner(a*u, v*n)*ds
    

    if (solve_method == 'Matrix'):
        M = assemble(Mass(u))
        F = assemble(RHS(u))
    else:
        lhs = Mass(u) - (1 - theta)*dt*RHS(u)
        rhs = Mass(u0) + theta*dt*RHS(u0)

    t = dt
    uh = Function(V_dg)
    def theta_integrator(t, dt):
        gx.t = t
        gy.t = t
        if (solve_method == 'Matrix'):
            b = (M + dt*theta*F)*u0.vector()
            A = M - (1 - theta)*dt*F
            [bc.apply(A,b) for bc in bcs]
            solve(A, uh.vector(), b)
        else:
            solve(lhs == rhs, uh, bcs)
            #solve(lhs == rhs, uh, bcs, \
                  #solver_parameters={"linear_solver": "gmres"})

        t += dt 
        u0.assign(uh)
        return t
    
    while t < T:
        t = theta_integrator(t, dt)
    if (T + dt - t) > tol:
        t = theta_integrator(T, T+dt - t)

    # Compute the error in L2, Linf norm
    # Define the exact solution
    u_ex = Expression('sin(2*pi*(x[0] + x[1] - (ax+ay)*t))', ax=ax, ay=ay, t=T, degree=2*degree)
    # interpolate the exact solution to high order space
    Ve = FunctionSpace(mesh, 'DG', 2*degree)
    u_ex_Ve = interpolate(u_ex, Ve)
    # This approach may not stable 
    error = (uh - u_ex_Ve)**2*dx
    err2 = sqrt(assemble(error))
    # This approach gives a warning!
    #err2 = errornorm(u, u_ex_Ve, 'l2', degree_rise=degree, mesh=mesh)
    uh_Ve = interpolate(uh, Ve)
    erri = abs(u_ex_Ve.vector().array() - uh_Ve.vector().array()).max()


    # Dump solution to file in VTK format
    #file = File(method+"_linear.pvd")
    #file << u

    # Plot solution and mesh
    #if (nx == 40):
        #plot(uh)
        ### Hold plot
        #interactive()
    if (norm == 'L2'):
    #if (norm == 'Linf'):
        return erri
    else: 
        return err2

def convergence_test(degree=1, flux='upwind', theta=1.):
    h = []
    err = []
    for nx in [5, 10, 20, 40]:
        h.append(1./nx)
        err.append(linear_convection(nx, nx, degree, flux, theta))

    print ('degree = %i' % degree)
    print ('h = %8.2E error = %8.2E' % (h[0], err[0]))
    for i in range(1, len(err)):
        order = ln(err[i]/err[i-1])/ln(h[i]/h[i-1])
        print ('h = %8.2E error = %8.2E order = %.2f' % (h[i], err[i], order))


def main(argv):
    if len(argv) == 3:
        degree = int(argv[0])
        flux = argv[1]
        theta = float(argv[2])
    elif len(argv) == 2:
        degree = int(argv[0])
        flux = argv[1]
        theta = 1.
    elif len(argv) == 1:
        degree = int(argv[0])
        flux = "upwind"
        theta = 1.
    else:
        degree = 1
        flux = "upwind"
        theta = 1.

    convergence_test(degree, flux, theta)
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
