"""This code solves 2D state linear equation
    dot(a, grad(u)) = f(x, y), 
on the unit square with source f given by
    f(x, y) = 0
and Dirichlet boundary conditions given by
    u(x, 0) = sin(2*pi*x), u(0, y) = -sin(2*pi*y)
using a discontinuous Galerkin formulation. 
For a = (1, 1), the exact solution
    u(x, y) = sin(2*pi*(x - y))
Note:
    works well for the degree from 1 to 4
FIXME:
    add flux choic from command line
    proper set the quad_degree (minimum)
"""
"""
    by Xiaozhou Li, June 3, 2016 
"""
from dolfin import *
import numpy as np
import sys

# Define parameter
tol = 1.e-14
a = Constant((1.0,1.0))
theta = 0.65
flux = "upwind"
#flux = "biupwind"
# Define the quadrature degree
quad_degree = 8
dx = dx(metadata={'quadrature_degree': quad_degree})

# Define the problem
def state_linear(nx, ny, degree, norm='L2'):
    # Create mesh and define function space
    mesh = UnitSquareMesh(nx, ny)
    print (degree)
    V_dg = FunctionSpace(mesh, 'DG', degree)

    
    # Define test and trial functions
    u = TrialFunction(V_dg)
    v = TestFunction(V_dg)

    # Define normal vector and mesh size
    n = FacetNormal(mesh)
    h = CellSize(mesh)
    h_avg = (h('+') + h('-')) / 2.

    # Define the exact solution
    def u_exact(x):
        return sin(2*pi*(x[0] - x[1]))
    # Define boundary conditions
    gy = Expression('sin(2*pi*x[0])', degree=2*degree)
    gx = Expression('-sin(2*pi*x[1])', degree=2*degree)
    #bcmethod = "geometric"
    def y_boundary(x, on_boundary):
        return on_boundary and x[0] < tol
    def x_boundary(x, on_boundary):
        return on_boundary and x[1] < tol
    inflow_y = DirichletBC(V_dg, gy, x_boundary, "geometric")
    inflow_x = DirichletBC(V_dg, gx, y_boundary, "geometric")
    bcs = [inflow_x, inflow_y]

    # Define the source function 
    f = Constant(0.0)

    # Define flux parameters
    alpha = Constant(1.0)

    # Define variational problem add our flux
    r = - inner(a*u,grad(v))*dx
    # diffusion flux
    if (flux == 'upwind'):
        f_hat = a * u('+')
        f_hat0 = a * u
    elif (flux == 'average'):
        f_hat = a * avg(u)
        f_hat0 = a * u
    elif (flux == 'downwind'):
        f_hat = a * u('-')
        f_hat0 = a * u
    elif (flux == 'biupwind'):
        f_hat = a * (theta*u('+') + (1 - theta)*u('-'))
        f_hat0 = a * u
    r += inner(f_hat, jump(v,n))*dS + inner(f_hat0, v*n)*ds
    L = f*v*dx

    # Compute solution
    u = Function(V_dg)
    solve(r==L, u, bcs)

    # Compute the error in L2, Linf norm
    u_ex = Expression('sin(2*pi*(x[0] - x[1]))', degree=2*degree)
    # interpolate the exact solution to high order space
    Ve = FunctionSpace(mesh, 'DG', 2*degree)
    u_ex_Ve = interpolate(u_ex, Ve)
    # This approach may not stable 
    error = (u - u_ex_Ve)**2*dx
    err2 = sqrt(assemble(error))
    # This approach gives a warning!
    #err2 = errornorm(u, u_ex_Ve, 'l2', degree_rise=degree, mesh=mesh)
    u_Ve = interpolate(u, Ve)
    erri = abs(u_ex_Ve.vector().array() - u_Ve.vector().array()).max()


    # Dump solution to file in VTK format
    #file = File(method+"_poisson.pvd")
    #file << u

    # Plot solution and mesh
    #if (nx == 80):
        #plot(u)
        ## Hold plot
        #interactive()
    if (norm == 'Linf'):
        return erri
    else: 
        return err2

def convergence_test(degree):
    h = []
    err = []
    for nx in [8, 16, 32, 64]:
        h.append(1./nx)
        err.append(state_linear(nx, nx, degree))

    print ('degree = %i' % degree)
    print ('h = %8.2E error = %8.2E' % (h[0], err[0]))
    for i in range(1, len(err)):
        order = ln(err[i]/err[i-1])/ln(h[i]/h[i-1])
        print ('h = %8.2E error = %8.2E order = %.2f' % (h[i], err[i], order))


def main(argv):
    if len(argv) == 1:
        degree = int(argv[0])
    else:
        degree = 1

    convergence_test(degree)
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
