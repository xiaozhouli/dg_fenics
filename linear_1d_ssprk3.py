"""This code solves 1D linear equation
    u_t + a*u_x = f(x), 
on the unit interval with source f given by
    f(x) = 0
and Dirichlet boundary conditions given by
    u(x, t) = sin(2*pi*(x-a*t)), 
initial condition
    u(x, 0) = sin(2*pi*x)
using a discontinuous Galerkin formulation. 
The exact solution
    u(x, t) = sin(2*pi*(x -a*t))
Note:
    works well for the degree from 1 to 4
    time integrator: SSPRK
    numerical flux: 
        1) upwind
        2) average wind
        3) downwind (unstable)
        4) biupwind
FIXME:
    add the source term into the varation form
    proper set the quad_degree (minimum)
"""
"""
    by Xiaozhou Li, March 30, 2017
"""
from dolfin import *
import numpy as np
import sys
import time

import SDG

set_log_level(30)
#set_log_active(False)
# Define parameter
tol = 1.e-8
a = 1.0
theta = 0.65 # used in biupwind flux, theta >= 0.5 (stable)
T  = 1
cfl = 0.05

# Define the quadrature degree
quad_degree = 10
parameters['form_compiler']['quadrature_degree'] = 8
#dx = dx(metadata={'quadrature_degree': quad_degree})

scheme = "explicit"
# Define the problem
def linear_convection(n, degree, flux, norm='L2'):
    # Create mesh and define function space
    mesh = UnitIntervalMesh(n)
    dt = cfl/n 
    print (flux, degree, dt)
    V_dg = FunctionSpace(mesh, 'DG', degree)
    
    # Define test and trial functions
    u = TrialFunction(V_dg)
    v = TestFunction(V_dg)


    # Define initial condition
    g0 = Expression('sin(2*pi*(x[0]))', degree=2*degree)
    u0 = project(g0, V_dg)
    #u0 = interpolate(g0, V_dg)

    uh = Function(V_dg)
    u1 = Function(V_dg)
    u2 = Function(V_dg)

    # Define Dirichlet boundary condition
    g = Expression('sin(2*pi*(x[0]-a*t))', a=a, t=0, degree=2*degree)
    
    # Note only "pointwise" option works for DG in 1D
    inflow_x = DirichletBC(V_dg, g, "near(x[0],0.0)", "pointwise")
    bcs = [inflow_x]

    # Define the source function 
    f = Constant(0.0)

    # Define variational problem add our flux
    def Jump(u):
        return u('+') - u('-')

    def Flux(u):
        if (flux == 'upwind'):
            f_hat = a * u('+')
            f_hat0 = a * u
        elif (flux == 'average'):
            f_hat = a * avg(u)
            f_hat0 = a * u
        elif (flux == 'downwind'):
            f_hat = a * u('-')
            f_hat0 = a * u
        elif (flux == 'biupwind'):
            f_hat = a * (theta*u('+') + (1 - theta)*u('-'))
            f_hat0 = a * u
        return f_hat, f_hat0

    def Mass(u):
        return u*v*dx

    def RHS(u):
        f_hat, f_hat0 = Flux(u)
        return inner(a*u,v.dx(0))*dx \
                - inner(f_hat, Jump(v))*dS \
                - inner(f_hat0, v)*ds
    
    def EulerRHS(dt, u):
        return Mass(u) + dt*RHS(u)

    LHS = Mass(u)
    RHS_1 = Mass(u0) + dt*RHS(u0)
    RHS_2 = 0.75*Mass(u0) + 0.25*(Mass(u1) + dt*RHS(u1))
    RHS_3 = 1./3.*Mass(u0) + 2./3.*(Mass(u2) + dt*RHS(u2))
    
    # SSPRK 3
    def SSPRK3(t, dt):
        g.t = t
        solve(LHS==RHS_1, u1, bcs)

        g.t = t - 0.5*dt
        solve(LHS==RHS_2, u2, bcs)

        g.t = t 
        solve(LHS==RHS_3, uh, bcs)

        t += dt
        u0.assign(uh)
        return t

    
    #if (n == 40):
        #file = File("./data/uh.pvd")
        #file << u0, 0.

    t = dt
    while t < T:
        t = SSPRK3(t, dt)
        plot(uh)
        #if (n == 40):
            #file << u0, t
    if (T + dt - t) > tol:
        t = SSPRK3(t, dt)

    # Compute the error in L2, Linf norm
    # Define the exact solution
    Ve = FunctionSpace(mesh, 'DG', 2*degree)
    u_ex = Expression('sin(2*pi*(x[0] - a*t))', a=a, t=T, degree=2*degree)
    u_ex = Expression('sin(2*pi*(x[0] - a*t))', a=a, t=T,\
                      element=Ve.ufl_element())
    # interpolate the exact solution to high order space
    u_ex_Ve = interpolate(u_ex, Ve)
    # This approach may not stable 
    error = (uh - u_ex)**2*dx
    #error = uh**2*dx - 2*uh*u_ex*dx +u_ex**2*dx(domain=mesh)
    #u**2*dx - 2*u*uh*dx + uh**2*dx
    err2 = sqrt(assemble(error))
    # This approach gives a warning!
    #err2 = errornorm(uh, u_ex_Ve, 'l2', degree_rise=degree, mesh=mesh)
    uh_Ve = interpolate(uh, Ve)
    erri = abs(u_ex_Ve.vector().array() - uh_Ve.vector().array()).max()


    # Dump solution to file in VTK format
    #file = File(method+"_linear.pvd")
    #file << u

    # Plot solution and mesh
    #if (n == 40):
        #plot(uh)
        #### Hold plot
        #interactive()
    if (norm == 'Linf'):
        return erri
    else: 
        return err2

def convergence_test(degree, flux):
    h = []
    err = []
    #for n in [5, 10, 20, 40, 80]:
    #for n in [40, 80]:
    for n in [5, 10, 20, 40]:
    #for n in [5, 10, 20]:
        h.append(1./n)
        err.append(linear_convection(n, degree, flux))

    print ('degree = %i' % degree)
    print ('h = %8.2E error = %8.2E' % (h[0], err[0]))
    for i in range(1, len(err)):
        order = ln(err[i]/err[i-1])/ln(h[i]/h[i-1])
        print ('h = %8.2E error = %8.2E order = %.2f' % (h[i], err[i], order))


def main(argv):
    if len(argv) == 2:
        degree = int(argv[0])
        flux = argv[1]
    elif len(argv) == 1:
        degree = int(argv[0])
        flux = "upwind"
    else:
        degree = 1
        flux = "upwind"

    convergence_test(degree, flux)
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
