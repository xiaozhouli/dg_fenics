"""This code solves 2D state linear equation
    u_t + dot(a, grad(u)) = f(x, y), 
on the unit square with source f given by
    f(x, y) = 0
and Dirichlet boundary conditions given by
    u(x, 0, t) = sin(2*pi*(x-(ax + ay)*t)), 
    u(0, y, t) = sin(2*pi*(y-(ax + ay)*t))
initial condition
    u(x, y, 0) = sin(2*pi*(x+y))
using a discontinuous Galerkin formulation. 
For a = (ax, ay), the exact solution
    u(x, y, t) = sin(2*pi*(x + y - (ax + ay)*t))
Note:
    works well for the degree from 1 to 4
    time integrator: SSP Runge-Kutta 3
        it is the most popular integrator for DG methods, however, it is not
        good enough for high order discretization
    numerical flux: 
        1) upwind
        2) average wind
        3) downwind (unstable)
        4) biupwind
FIXME:
    add the source term into the varation form
    proper set the quad_degree (minimum)
"""
"""
    by Xiaozhou Li, June 10, 2016 
"""
from dolfin import *
import numpy as np
import sys
import time

set_log_level(30)
#set_log_active(False)
# Define parameter
tol = 1.e-14
ax = 1.0; ay = 1.0; a = Constant((ax,ay))
theta = 0.65 # used in biupwind flux, theta >= 0.5 (stable)
T  = 0.1
cfl = 0.1

# Define the quadrature degree
quad_degree = 8
dx = dx(metadata={'quadrature_degree': quad_degree})

# Define the problem
def linear_convection(nx, ny, degree, flux, method, norm='L2'):
    # Create mesh and define function space
    mesh = UnitSquareMesh(nx, ny)
    dt = cfl/nx 
    print (flux, degree, dt)
    V_dg = FunctionSpace(mesh, 'DG', degree)
    
    # Define test and trial functions
    u = TrialFunction(V_dg)
    v = TestFunction(V_dg)

    # Define normal vector and mesh size
    n = FacetNormal(mesh)
    h = CellSize(mesh)
    h_avg = (h('+') + h('-')) / 2.


    # Define initial condition
    g0 = Expression('sin(2*pi*(x[0]+x[1]))', degree=2*degree)
    u0 = project(g0, V_dg)

    u1 = Function(V_dg)
    u2 = Function(V_dg)
    uh = Function(V_dg)

    # Define boundary conditions
    gy = Expression('sin(2*pi*(x[0]-(ax+ay)*t))', ax=ax, ay=ay, t=0, degree=2*degree)
    gx = Expression('sin(2*pi*(x[1]-(ax+ay)*t))', ax=ax, ay=ay, t=0, degree=2*degree)
    
    #bcmethod = "geometric"
    def y_boundary(x, on_boundary):
        return on_boundary and x[0] < tol
    def x_boundary(x, on_boundary):
        return on_boundary and x[1] < tol
    inflow_y = DirichletBC(V_dg, gy, x_boundary, "geometric")
    inflow_x = DirichletBC(V_dg, gx, y_boundary, "geometric")
    bcs = [inflow_x, inflow_y]

    # Define the source function 
    f = Constant(0.0)

    # Define variational problem add our flux
    def Flux(u):
        if (flux == 'upwind'):
            f_hat = a * u('+')
            f_hat0 = a * u
        elif (flux == 'average'):
            f_hat = a * avg(u)
            f_hat0 = a * u
        elif (flux == 'downwind'):
            f_hat = a * u('-')
            f_hat0 = a * u
        elif (flux == 'biupwind'):
            f_hat = a * (theta*u('+') + (1 - theta)*u('-'))
            f_hat0 = a * u

        return f_hat, f_hat0

    def Mass(u):
        return u*v*dx

    def RHS(u):
        f_hat, f_hat0 = Flux(u)

        return inner(a*u,grad(v))*dx - inner(f_hat, jump(v,n))*dS - inner(f_hat0, v*n)*ds
    
    # avoid assembling
    if method == 'MATRIX':
        M = assemble(Mass(u))
        F = assemble(RHS(u))
    else:
        LHS = Mass(u)
        RHS_1 = Mass(u0) + dt*RHS(u0)
        RHS_2 = 0.75*Mass(u0) + 0.25*(Mass(u1) + dt*RHS(u1))
        RHS_3 = 1./3.*Mass(u0) + 2./3.*(Mass(u2) + dt*RHS(u2))
    
    # SSPRK 3
    def SSPRK3(t, dt):
        gx.t = t
        gy.t = t
        if method == 'MATRIX':
            b = (M + dt*F)*u0.vector()
            [bc.apply(M,b) for bc in bcs]
            solve(M, u1.vector(), b)
        else:
            solve(LHS==RHS_1, u1, bcs)

        gx.t = t - 0.5*dt
        gy.t = t - 0.5*dt
        if method == 'MATRIX':
            b = 0.75*M*u0.vector() + 0.25*(M + dt*F)*u1.vector()
            [bc.apply(M,b) for bc in bcs]
            solve(M, u2.vector(), b)
        else:
            solve(LHS==RHS_2, u2, bcs)

        gx.t = t 
        gy.t = t 
        if method == 'MATRIX':
            b = 1./3.*M*u0.vector() + 2./3.*(M + dt*F)*u2.vector()
            [bc.apply(M,b) for bc in bcs]
            solve(M, uh.vector(), b)
        else:
            solve(LHS==RHS_3, uh, bcs)

        t += dt
        u0.assign(uh)
        return t

    t = dt
    while t < T:
        t = SSPRK3(t, dt)
    if (T + dt - t) > tol:
        t = SSPRK3(t, dt)

    # Compute the error in L2, Linf norm
    # Define the exact solution
    u_ex = Expression('sin(2*pi*(x[0] + x[1] - (ax+ay)*t))', ax=ax, ay=ay, t=T, degree=2*degree)
    # interpolate the exact solution to high order space
    Ve = FunctionSpace(mesh, 'DG', 2*degree)
    u_ex_Ve = interpolate(u_ex, Ve)
    # This approach may not stable 
    error = (uh - u_ex_Ve)**2*dx
    err2 = sqrt(assemble(error))
    # This approach gives a warning!
    #err2 = errornorm(u, u_ex_Ve, 'l2', degree_rise=degree, mesh=mesh)
    uh_Ve = interpolate(uh, Ve)
    erri = abs(u_ex_Ve.vector().array() - uh_Ve.vector().array()).max()


    # Dump solution to file in VTK format
    #file = File(method+"_linear.pvd")
    #file << u

    # Plot solution and mesh
    #if (nx == 40):
        #plot(uh)
        ### Hold plot
        #interactive()
    if (norm == 'Linf'):
        return erri
    else: 
        return err2

def convergence_test(degree, flux, method='MATRIX'):
    h = []
    err = []
    for nx in [5, 10, 20, 40, 80]:
        h.append(1./nx)
        err.append(linear_convection(nx, nx, degree, flux, method))

    print ('degree = %i' % degree)
    print ('h = %8.2E error = %8.2E' % (h[0], err[0]))
    for i in range(1, len(err)):
        order = ln(err[i]/err[i-1])/ln(h[i]/h[i-1])
        print ('h = %8.2E error = %8.2E order = %.2f' % (h[i], err[i], order))


def main(argv):
    if len(argv) == 2:
        degree = int(argv[0])
        flux = argv[1]
    elif len(argv) == 1:
        degree = int(argv[0])
        flux = "upwind"
    else:
        degree = 1
        flux = "upwind"

    convergence_test(degree, flux)
    # measure the time of two different implementations 
    #for method in ['MATRIX', 'PDE']:
        #print ("METHOD: %s" % method)
        #start = time.time()
        #convergence_test(degree, flux, method)
        #end = time.time()
        #print ("time: ", end - start)
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
